
import presentacion.Modelo;


/**
 *
 * @author jorge cardenas
 */
public class Principal {
    private Modelo miApp;
    
    public Principal(){
        miApp = new Modelo();
        miApp.iniciar();
    }
    public static void main(String[] args) {
        new Principal();
    }
}
